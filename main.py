import jinja2
import os

import webapp2
import logging
import json

from datetime import datetime, timedelta
from google.appengine.api import users

from model import *
from routing import *

APIKEY = 'AoU6W6JpviTMydpMpIB3uTTesLJxuVhkZQBdyHu8hjWr42GkSme4iplHORBd8SQi'

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class Home(webapp2.RequestHandler):
    def get(self):
        user_routes = {}
        if users.get_current_user():
            user = User.get_by_id(users.get_current_user().user_id())
            if user != None: user_routes = user.routes
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Log in to Google'
        template_values = {
            'url': url,
            'url_linktext': url_linktext,
            'apikey': APIKEY,
            'user_routes': user_routes
        }
        template = jinja_environment.get_template('index.html')
        self.response.out.write(template.render(template_values))

class RouteWriter(webapp2.RequestHandler):
    def post(self):
        self.response.headers['Content-Type'] = 'application/json'
        src = self.request.get('src')
        dst = self.request.get('dst')
        srcaddr = self.request.get('srcaddr')
        dstaddr = self.request.get('dstaddr')
        duration = int(self.request.get('dur'))
        user_routes = []
        
        userid = None
        if users.get_current_user():
            userid = users.get_current_user().user_id()
            
        success = None
        if src and dst and duration:
            success = Route.create(src, dst, duration, srcaddr, dstaddr, userid)
        
        if userid != None:
            user = User.get_by_id(userid)
            if user.routes != None:
                for r in user.routes:
                    user_routes.append({ 'label': r.label_to_string(), 'srcaddr': r.srcaddr, 'dstaddr': r.dstaddr })

        self.response.out.write(json.dumps(user_routes))

class RouteUpdater(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'

        logging.debug('--- route updater: started')

        routes = Route.for_update()

        rr = RouteRequest(apikey=APIKEY)
        args = {'optmz': 'timeWithTraffic' }

        logging.debug('--- routes:')
        logging.debug(routes)

        for rte in routes:
            src = rte.src_to_string()
            dst = rte.dst_to_string()
            url, jsn = rr.route(waypoints=[src, dst], **args)
            if jsn != None:
                self.response.out.write(url + "\n")
                try:
                    duration = int(jsn['resourceSets'][0]['resources'][0]['travelDuration'])
                    success = Route.update(rte.get_key_name(), duration)
                    self.response.out.write("duration: " + str(duration) + "\n")
                    self.response.out.write("success: " + str(success) + "\n")
                except Exception as e:
                    logging.error(e)
                    self.response.out.write(e)
                    self.response.out.write("\n")
                    self.response.out.write(jsn)
                    self.response.out.write("\n")
            else:
                self.response.out.write('Error: ' + url)
                self.response.out.write("\n")

        self.response.out.write("OK\n\n")
        logging.debug('--- route updater: done')

class RouteReader(webapp2.RequestHandler):
    def get(self):
        self.route_stats()
    
    def post(self):
        self.route_stats()

    def route_stats(self):
        self.response.headers['Content-Type'] = 'application/json'
        src = self.request.get('src')
        dst = self.request.get('dst')
        tz = int(self.request.get('tz'))
        
        stats = {}
        if src and dst:
            instances = Route.statistics(src, dst)
            if instances != None and len(instances) > 0:
                for h in range(24):
                    for m in range(0, 60, 15):
                        stats[(h, m)] = []

                for i in instances:
                    h = (i.created.hour + tz) % 23
                    m = (int(i.created.minute) / 15) * 15
                    totalminutes = int(float(i.duration) / 60)
                    stats[(h, m)].append(totalminutes)

        data = []
        predict = []

        for t in sorted(stats):
            avg = 0
            if len(stats[t]) > 0:
                avg = sum(stats[t]) / len(stats[t])
            data.append([t[0], t[1], len(stats[t]), avg])

        current_time = datetime.utcnow() + timedelta(hours=tz)
        current_block = Route.tblock(current_time)
        total_blocks = len(data)
        for i in range(1, 4):
            next_block = (current_block + i) % total_blocks
            predict.append(data[next_block][3])

        result = { 'data': data, 'predict': predict };
        
        self.response.out.write(json.dumps(result))

class RouteUtility(webapp2.RequestHandler):
    def get(self):
        action = self.request.get('eh')
        self.response.headers['Content-Type'] = 'text/plain'
        if action == 'um':
            routes = Route.query()
            for rte in routes:
                rte.update_missing()
                self.response.out.write('Updated ' + rte.get_key_name() + "\n")
            self.response.out.write("Done updating\n")
        if action == 'lr':
            routes = Route.query()
            for key in routes.iter(keys_only=True):
                self.response.out.write(str(key) + "\n")
            self.response.out.write("Done\n")

app = webapp2.WSGIApplication([('/', Home), 
    ('/route', RouteWriter), 
    ('/uproute', RouteUpdater), 
    ('/stats', RouteReader), 
    ('/xyz', RouteUtility)], debug=True)

def main():
    logging.getLogger().setLevel(logging.DEBUG)
    webapp.util.run_wsgi_app(app)

if __name__ == '__main__':
    main()