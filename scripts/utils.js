function createTimezones() {
    var $tz = $("#tz");
    $tz.empty();
    for (var i = -11; i < 12; i++) {
        var txt = (i >= 0) ? 'GMT+' + i : 'GMT' + i;
        $tz.append($('<option></option>').attr("value", i).text(txt));
    }
    var offset = new Date().getTimezoneOffset();
    var offsetHours = (-offset) / 60;
    $tz.val(offsetHours);
}

$(document).ready(function() {
    createTimezones();
});
