google.load('visualization', '1.0', {'packages':['controls']});
google.setOnLoadCallback(initCharts);

var dashboard = null;
var slider = null;
var chartname = "Your route statistics";
var chart = null;

function clearCharts() {
    $('.stats').html('');
}

function initControls() {
    dashboard = new google.visualization.Dashboard(document.getElementById('dashboard'));
    slider = new google.visualization.ControlWrapper({
        'controlType': 'NumberRangeFilter',
        'containerId': 'slider',
        'options': { 'filterColumnLabel' : 'Duration', 'ui': { 'unitIncrement': 60 } }
    });
    chart = createChart(chartname);
    dashboard.bind(slider, chart);
}

function initCharts() {
    clearCharts();
    initControls();
}

function drawCharts(data) {
    var t = createDataTable(data);
    dashboard.draw(t);
}

function formatHint(hour, minute, totalminutes) {
    if (minute < 10) minute = '0' + minute;
    var tip = (hour > 12)
        ? 'at ' + (hour % 12) + ':' + minute + 'pm'
        : 'at ' + hour + ':' + minute + 'am';
    h = Math.floor(totalminutes / 60);
    m = Math.floor(totalminutes % 60);
    if (m < 10) m = '0' + m;
    if (h < 10) h = '0' + h;
    return tip + ', ' + h + ':' + m;
}

function createDataTable(data) {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn('timeofday', 'Hour');
    dataTable.addColumn('number', 'Duration');
    dataTable.addColumn({ type: 'string', role: 'tooltip' });

    if (data != null) {
        for (var i = 0; i < data.length; i++) {
            var h = data[i][0];
            var m = data[i][1];
            var v = data[i][3];
            var tip = formatHint(h, m, v);
            dataTable.addRow([[h, m, 0, 0], v, tip]);
        }
    }
    
    return dataTable;
}

function createChart(charttitle) {
    var chart = new google.visualization.ChartWrapper({
            chartType: 'BarChart', 
            options: {
                title: charttitle,
                legend: 'none',
                hAxis: { title: 'Duration (minutes)' },
                vAxis: { title: 'Time of day', direction: -1 },
                width: '90%',
                height: 1200,
                chartArea: { left: 100, top: 50, height: 1050 }
            },
            containerId: 'stats'
        }
    );
    return chart;
}
