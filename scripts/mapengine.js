var map;
var directionsManager;
var DEBUG_ON = false;

function initialize() {
    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', { callback: initializeMap });
}

function initializeMap() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(locateMap);
    }
    else {
        locateMap(null);
    }
}

function showRoute(srcaddr, dstaddr) {
    if ($('#rt_src').val() != srcaddr || $('#rt_dst').val() != dstaddr) {
        $('#rt_src').val(srcaddr);
        $('#rt_dst').val(dstaddr);
        calcRoute(srcaddr, dstaddr);
    }
}

function locateMap(position) {
    var lat = (position) ? position.coords.latitude : 37.7699298;
    var lon = (position) ? position.coords.longitude : -122.4469157;
    var options = {
        credentials: apikey,
        center: new Microsoft.Maps.Location(lat, lon),
        mapTypeId: Microsoft.Maps.MapTypeId.road,
        showMapTypeSelector: false,
        disablePanning: true,
        disableZooming: true,
        animate: true,
        zoom: 11
    };
    map = new Microsoft.Maps.Map(document.getElementById("map"), options);
    directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);
    attachHandlers();
}

function error(msg) {
    $('.error p').html(msg)
    $('.error').css('display', 'block').fadeOut(2000);
}

function debug(msg) {
    if (!DEBUG_ON) return;
    $('.debug').html(msg);
}

function sq(s) {
    return "'" + s + "'";
}

function updateSavedRoutes(routes) {
    $('#your-routes').html('');
    for (var i = 0; i < routes.length; i++) {
        var onclickhtml = 'showRoute(' + sq(routes[i].srcaddr) + ', ' + sq(routes[i].dstaddr) + ')';
        $('<div class="button orange blk" onclick="' + onclickhtml + '" />').html(routes[i].label).appendTo('#your-routes');
    }
}

function showDuration(duration, suffix) {
    if (duration) {
        var h = Math.floor(duration / 3600);
        var m = Math.round((duration - (60 * h)) / 60);
        var txtDuration = m + ' minutes';
        if (h > 0) txtDuration = h + ' hours ' + txtDuration;
        $('#route-info-' + suffix).html(txtDuration);
        $('#duration-' + suffix).css('display', 'block')
    }
    else {
        $('#duration-' + suffix).css('display', 'none')
    }
}

function showPrediction(predict) {
    for (var i = 0; i < predict.length; i++) {
        var duration = predict[i] * 60; // min -> sec
        var suffix = 'traffic-' + ((i + 1) * 15);
        showDuration(duration, suffix);
    }
}

function showDistance(distance) {
    var txtDistance = distance.toFixed(1) + ' miles';
    $('#distance-info-current').html(txtDistance);
    $('#distance-current').css('display', 'block');
}

function directionsReceived(e) {
    if (!e) return;
    showDistance(e.routeSummary[0].distance);
    showDuration(e.routeSummary[0].time, 'current');
    showDuration(e.routeSummary[0].timeWithTraffic, 'traffic');

    var points = directionsManager.getAllWaypoints();
    
    $('#rt_src').val(points[0].getAddress());
    $('#rt_dst').val(points[1].getAddress());
    
    saveRouteData(
        points[0].getLocation(),
        points[1].getLocation(),
        e.routeSummary[0].timeWithTraffic,
        points[0].getAddress(),
        points[1].getAddress());

    readRouteStatistics(
        points[0].getLocation(),
        points[1].getLocation());
}

function calcRoute(src, dst) {
    directionsManager.resetDirections();
    var srcWaypoint = new Microsoft.Maps.Directions.Waypoint( { address: src } );
    var dstWaypoint = new Microsoft.Maps.Directions.Waypoint( { address: dst } );
    var requestOptions = {
        routeMode: Microsoft.Maps.Directions.RouteMode.driving,
        routeOptimization: 'shortestTime',
        avoidTraffic: 'true'
    };
    directionsManager.addWaypoint(srcWaypoint);
    directionsManager.addWaypoint(dstWaypoint);

    directionsManager.setRequestOptions(requestOptions);
    directionsManager.calculateDirections();
}

function saveRouteData(srcloc, dstloc, duration, srcaddr, dstaddr) {
    var d = {
        'src': srcloc.latitude + ',' + srcloc.longitude,
        'dst': dstloc.latitude + ',' + dstloc.longitude,
        'dur': duration,
        'srcaddr': srcaddr,
        'dstaddr': dstaddr
    };
    var request = $.ajax({
        type: 'POST',
        url: '/route',
        data: d,
        success: function(result, status, jqXHR) {
            debug(JSON.stringify(result));
            updateSavedRoutes(result);
        },
        error: function(status, jqXHR) {
            debug("ERROR: " + status);
        }
    });
}

function readRouteStatistics(srcloc, dstloc) {
    var src = to6dec(srcloc.latitude) + ',' + to6dec(srcloc.longitude);
    var dst = to6dec(dstloc.latitude) + ',' + to6dec(dstloc.longitude);
    var tzo = $('#tz').val();
    var d = {
        'src': src,
        'dst': dst,
        'tz': tzo
    };
    var request = $.ajax({
        type: 'POST',
        url: '/stats',
        data: d,
        success: function(result, status, jqXHR) {
            debug(JSON.stringify(result));
            drawCharts(result.data);
            showPrediction(result.predict);
        },
        error: function(status, jqXHR) {
            debug("ERROR: " + status);
        }
    });
}

function to6dec(coord) {
    var r = coord.toFixed(6);
    return r;
}

function validate(src, dst) {
    if (!src) {
        $('#rt_src').focus();
        error('Please enter source address.');
        return false;
    }
    if (!dst) {
        $('#rt_dst').focus();
        error('Please enter destination address.');
        return false;
    }
    if (src == dst) {
        $('#rt_dst').focus();
        error('Please use different source and destination addresses.');
        return false;
    }
    return true;
}

function attachHandlers() {
    
    Microsoft.Maps.Events.addHandler(directionsManager, 'directionsError', function(e) { error(e.message); });
    Microsoft.Maps.Events.addHandler(directionsManager, 'directionsUpdated', directionsReceived);

    var f = function(e) {
        if (e.which == 13) {
            var src = $('#rt_src').val();
            var dst = $('#rt_dst').val();
            if (validate(src, dst)) {
                calcRoute(src, dst);
            }
        }  
    }

    $('#rt_src').keydown(f);
    $('#rt_dst').keydown(f);
}

$(document).ready(function() {
    initialize();
});
