This website was created with Python and is hosted in Google App Engine.

It uses:

- Bing Maps Ajax Control 7.0
- Bing Maps API as the back-end data source
- jninja2 as the templating engine

URL in Google App Engine:

http://best-time-to-go.appspot.com