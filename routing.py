import urllib
import urllib2
import logging
import json

class RouteError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class BingRequest(object):

    BASE_URI = 'http://dev.virtualearth.net/REST'
    BASE_URI_SSL = 'https://dev.virtualearth.net/REST'
    DEFAULT_VERSION = 1

    apikey = None
    referer = None
    culture = None
    version = None
    restapi = None
    usessl = False
    
    params = {}

    last_response_code = None

    def __init__(self, apikey=None, restapi=None, referer=None, culture=None, version=None, usessl=False):
        self.apikey = apikey
        self.referer = referer
        self.restapi = restapi
        self.usessl = usessl

        if (version):
            self.version = version
        else:
            self.version = self.DEFAULT_VERSION
    
    def url(self):
        baseuri = self.BASE_URI
        if self.usessl:
            baseuri = self.BASE_URI
        ps = '&'.join(map(self.param, sorted(self.params)))
        uri = '%s/v%d/%s?%s&key=%s' % (baseuri, self.version, self.restapi, ps, self.apikey)
        return uri
    
    def param(self, key):
        return key + '=' + urllib.quote_plus(self.params[key])

    def retrieve_json(self, url):
        try:
            req = urllib2.Request(url)
            req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4')
            res = urllib2.urlopen(req)
            self.last_response_code = res.getcode()
            if self.last_response_code != 200:
                return url, None
            return url, json.load(res)
        except Exception as e:
            logging.error(e)
            return url, None

    def retrieve_json_test(self, url):
        import codecs
        res = codecs.open('response.txt', 'r', 'utf-8')
        return url, json.load(res)

class RouteRequest(BingRequest):

    waypoints = []

    DEFAULT_TRAVEL_MODE = 'Driving'
    DEFAULT_OUTPUT = 'json'

    TRAVEL_MODES = set(['Driving', 'Walking', 'Transit'])
    OPTIMIZE = set(['distance', 'time', 'timeWithTraffic'])
    OUTPUT = set(['json', 'xml'])
    
    ERROR_WAYPOINTS_LIST = 'waypoints should be a list'
    ERROR_WAYPOINTS_MORE = 'waypoints should be a list of 2 or more elements'
    ERROR_TRAVEL_MODE = 'possible travelMode values: ' + '|'.join(TRAVEL_MODES)
    ERROR_OPTIMIZE = 'possible optmz values: ' + '|'.join(OPTIMIZE)
    ERROR_OUTPUT = 'possible o values: ' + '|'.join(OUTPUT)

    def __init__(self, apikey=None, referer=None, culture=None, version=None, usessl=None):
        BingRequest.__init__(self, apikey, 'Routes/Driving', referer, culture, version, usessl)

    def route(self, waypoints=None, travelMode='Driving', **kwargs):
        if not isinstance(waypoints, list):
            raise RouteError(self.ERROR_WAYPOINTS_LIST)
        
        if waypoints == None or len(waypoints) < 2:
            raise RouteError(self.ERROR_WAYPOINTS_MORE)
        
        if travelMode not in self.TRAVEL_MODES:
            raise RouteError(self.ERROR_TRAVEL_MODE)
        
        if 'optmz' in kwargs and kwargs['optmz'] not in self.OPTIMIZE:
            raise RouteError(self.ERROR_OPTIMIZE)

        if 'o' in kwargs and kwargs['o'] not in self.OUTPUT:
            raise RouteError(self.ERROR_OUTPUT)
        
        if 'o' not in kwargs:
            kwargs['o'] = self.DEFAULT_OUTPUT
        
        self.restapi = 'Routes/' + travelMode
        self.params = kwargs
        
        for i in range(len(waypoints)):
            self.params['wp.' + str(i)] = waypoints[i]

        return self.retrieve_json(self.url())
