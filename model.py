from datetime import datetime, timedelta
from google.appengine.ext import ndb

MISSING_BLOCKS = [i for i in range(96)] # 24 * 4
DATA_POINTS_PER_BLOCK = 5

class RouteInfo(ndb.Model):
    duration = ndb.IntegerProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

class UserRoute(ndb.Model):
    routekey = ndb.StringProperty()
    label = ndb.StringProperty(default='')
    srcaddr = ndb.StringProperty()
    dstaddr = ndb.StringProperty()

    def label_to_string(self):
        if self.label == '':
            return 'From %s to %s' % (self.srcaddr, self.dstaddr)
        return self.label

    @classmethod
    def create(self, userid, routekey, srcaddr, dstaddr):
        user = User.get_or_insert(userid)
        if user:
            route = UserRoute(routekey=routekey, srcaddr=srcaddr, dstaddr=dstaddr)
            if user.routes and len(user.routes) > 0:
                if not any([r for r in user.routes if r.routekey == routekey]):
                    user.routes.append(route)
            else:
                user.routes = [route]
            user.put()

class User(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    routes = ndb.StructuredProperty(UserRoute, repeated=True)

class Route(ndb.Model):
    src = ndb.GeoPtProperty()
    dst = ndb.GeoPtProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    latest = ndb.DateTimeProperty(auto_now_add=True)
    instances = ndb.StructuredProperty(RouteInfo, repeated=True)
    missing = ndb.IntegerProperty(repeated=True)

    def src_to_string(self):
        return ('%f,%f') % (self.src.lat, self.src.lon)

    def dst_to_string(self):
        return ('%f,%f') % (self.dst.lat, self.dst.lon)

    def get_key_name(self):
        key = "%f,%f,%f,%f" % (self.src.lat, self.src.lon, self.dst.lat, self.dst.lon)
        return key

    def update_missing(self):
        times = map(lambda x: Route.tblock(x.created), self.instances)
        freqs = {}
        for t in MISSING_BLOCKS: freqs[t] = 0
        for t in times: freqs[t] += 1
        self.missing = [t for t in freqs if freqs[t] < DATA_POINTS_PER_BLOCK]

    def is_new_data_point(self, t):
        l = len(self.instances)
        if l > 0:
            last = self.instances[l-1].created
            return t.year != last.year or t.month != last.month or t.day != last.day or Route.tblock(t) != Route.tblock(last)
        return True

    def add_instance(self, duration):
        current_time = datetime.utcnow()
        added = False
        if self.instances and len(self.instances) > 0:
            if self.is_new_data_point(current_time):
                self.instances.append(RouteInfo(duration=duration, created=current_time))
                added = True
        else:
            self.instances = [RouteInfo(duration=duration, created=current_time)]
            added = True
        if added: self.update_missing()
        return added

    @classmethod
    def tblock(self, t):
        total_minutes = int(t.hour) * 60 + int(t.minute)
        return total_minutes / 15

    @classmethod
    def to_geopt(self, s):
        lat, lon = map(float, s.split(','))
        return ndb.GeoPt(lat, lon)

    @classmethod
    def create(self, src, dst, duration, srcaddr, dstaddr, userid=None):
        s = Route.to_geopt(src)
        d = Route.to_geopt(dst)
        key_name = "%f,%f,%f,%f" % (s.lat, s.lon, d.lat, d.lon)
        rte = Route.get_or_insert(key_name, src=s, dst=d)
        if rte:
            if userid:
                UserRoute.create(userid=userid, routekey=key_name, srcaddr=srcaddr, dstaddr=dstaddr)
            rte.latest = datetime.utcnow()
            rte.add_instance(duration)
            rte.put()
            return True

        return False

    @classmethod
    def for_update(self):
        current_time = datetime.utcnow()
        current_block = Route.tblock(current_time)
        routes = Route.query(Route.missing == current_block).fetch(projection = ['src', 'dst'])
        return routes

    @classmethod
    def update(self, key_name, duration):
        rte = Route.get_by_id(key_name)
        if rte:
            if rte.add_instance(duration):
                rte.put()
            return True

        return False

    @classmethod
    def statistics(self, src, dst):
        key_name = "%s,%s" % (src, dst)
        rte = Route.get_by_id(key_name)
        if rte:
            return rte.instances
        return None
